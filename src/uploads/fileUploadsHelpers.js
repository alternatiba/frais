// New class with additional fields for Image
import {compressImageDataUrl} from "../helpers";

class CustomImage extends Image {
  constructor() {
    super();
  }

  // `imageType` is a required input for generating a PDF for an image.
  get imageType() {
    return this.mimeType.split("/")[1];
  }
}

export const fileToImageURL = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener(
      "load",
      async () => {
        // convert image file to base64 string
        const dataUrl = reader.result.toString();
        const compressedDataUrl = await compressImageDataUrl(dataUrl, file.type);

        resolve(compressedDataUrl);
      },
      false
    );

    reader.readAsDataURL(file);
  });
};

export const fileToPdfDocument = (file) => {
  const pdfUrl = URL.createObjectURL(file);
  return pdfjsLib.getDocument({url: pdfUrl});
};
