import {derived, writable} from "svelte/store";
import {organizations, defaultBillTitle, defaultMoneySign} from "../CONFIGURATION";

// Gets the next orga based on the current one
const getNextOrganization = (currentOrga) => {
  const currentIndex = organizations.findIndex((el) => el === currentOrga);
  return organizations[(currentIndex + 1) % organizations.length];
};

const currentOrgaStore = () => {
  // The hash is the https://xxxxx.xxxx/#part-here-behind-the-hash.
  // We check its value to determine on which orga we should launch the app

  const harmonizeString = (str) => str.replace(/ /g, "-").toLocaleLowerCase();

  const updateHashForOrga = (orga) => (window.location.hash = `#${harmonizeString(orga.name)}`);

  const initializeOrgaFromHash = (set) => {
    const urlHash = window.location.hash?.substring(1);
    if (urlHash) {
      // Find if the hash matches with any of our organizations
      const index = organizations.findIndex(
        (orga) => harmonizeString(orga.name) === window.location.hash.substring(1)
      );
      if (index !== -1) {
        // If yes, set this orga
        set(organizations[index]);
      } else {
        // Else fallback on the default orga
        set(organizations[0]);
        // In case hash is badly written, like "#zadzarezq", erase it
        history.replaceState(null, null, " ");
      }
    }
  };

  const {subscribe, update} = writable(organizations[0], initializeOrgaFromHash);

  return {
    subscribe,
    toggleNext: () =>
      update((currentOrga) => {
        const newOrga = getNextOrganization(currentOrga);
        updateHashForOrga(newOrga);
        return newOrga;
      }),
  };
};

/**
 * [ CURRENT ORGA ]
 *
 * Stores the current organization and the data from the configuration.
 *
 * - toggleNext() allows to switch to the next organization
 *
 * @see currentOrgaStore
 *
 * @link https://svelte.dev/tutorial/custom-stores
 */
export const currentOrga = currentOrgaStore();

/**
 * The next organization that will be taken if we toggle the organization
 *
 * @link https://svelte.dev/tutorial/derived-stores
 */
export const nextOrga = derived(currentOrga, getNextOrganization);

/**
 * The bill title
 */
export const billTitle = writable(defaultBillTitle);

/**
 *  * A default new row in the bill
 *
 * - type, description and amount are linked to the three inputs of the row
 *
 * - checked refers to whether it's checked in the confirmation modal
 *
 * - attachments is also about stuff that is checked or not (checking the attachment checkboxes in the modal)
 *
 * - uploadedImages contains Images that are beeing uploaded to this row
 *
 * - uploadedPdfs contains PDF files stored as PDF.js documents, so they can be displayed
 *
 * @type {{amount: undefined, attachments: *[], pdfsUploaded: *[], imagesUploaded: *[], description: string, checked: boolean, type: string}}
 */
const newRowTemplate = {
  type: "",
  description: "",
  amount: undefined,
  checked: false,
  attachments: undefined,
  uploadedImages: [],
  uploadedPdfs: [],
};

const inventoryRowsStore = () => {
  const {subscribe, update, set} = writable([{...newRowTemplate}]);

  return {
    subscribe,
    set,
    addRow: () => update((inventoryRows) => [...inventoryRows, {...newRowTemplate}]),
    removeRow: (rowToRemove) =>
      update((inventoryRows) => inventoryRows.filter((row) => rowToRemove !== row)),
  };
};
/**
 * [ INVENTORY ROWS ]
 *
 * All the rows of the bill, and what they contain.
 * @see newRowTemplate
 * @see inventoryRowsStore
 */
export const inventoryRows = inventoryRowsStore();

export const moneySign = writable(defaultMoneySign);

export const ribUpload = writable({pdf: [], image: [], iban: "", bic: "", bankName: ""});
