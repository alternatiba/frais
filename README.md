# Note de frais - Alternatiba / ANV-COP21

> Une micro-application pour faire des notes de frais en deu-spi

[[_TOC_]]

## Démo

[Alternatiba](https://alternatiba.eu) et [ANV-COP21](https://anv-cop21.org) utilisent déjà l'application. Vous pouvez la
voir en allant sur [**frais.alternatiba.eu**](https://frais.alternatiba.eu).

## Utiliser l'application

### Configuration

L'application a besoin d'un fichier [CONFIGURATION.js](CONFIGURATION.js) pour fonctionner. Or, comme vous devez le voir,
ce dernier n'existe pas ! Il va falloir le créer.

Heureusement, on a déjà créé un modèle de configuration type pour vous aider. Il est disponible dans le
fichier [CONFIGURATION.example.js](CONFIGURATION.example.js). Toutes les explications y sont détaillées.

Pour pouvoir lancer votre application, le plus simple c'est de copier-coller le modèle existant dans un nouveau fichier [CONFIGURATION.js](CONFIGURATION.js). Vous pouvez ensuite modifier le
fichier à votre convenance :

```bash
# Dans le dossier racine, copier-coller CONFIGURATION.js dans un nouveau fichier appelé CONFIGURATION.js 
cp CONFIGURATION.example.js CONFIGURATION.js

# Si vous êtes sur un serveur, ouvrez le fichier de configuration avec votre éditeur préféré pour le
# modifier à votre convenance (que ce soit nano ou un autre éditeur) :
nano CONFIGURATION.js
# Sinon, vous pouvez simplement prendre un éditeur texte classique.
```

### Déployer l'application en production

Pour utiliser l'application sur votre serveur, vous allez devoir la "builder". Deux options s'offrent à vous:

- Option 1: compiler l'application sur votre PC perso, et copier les fichiers créés sur votre serveur.
- Option 2: compiler l'application directement sur votre serveur.

Peu importe l'option choisie, vous allez devoir lancer la compilation comme suit :

- Si ce n'est pas déjà fait, installez `npm` sur la machine que vous utilisez (vous aurez peut-être besoin des droits
  super-utilisateur) :
    ```bash
    apt-get install npm 
    ```
- Installez les dépendances de l'application, puis lancez la compilation du projet :
    ```bash 
    npm install
    npm run build
    ```

L'application est alors compilée dans le dossier `/public`. Il suffira ensuite d'exposer le dossier `/public` sur le
serveur de production en tant que contenu statique. En effet, le point d'entrée est le fichier `/public/index.html`. Si
vous avez choisi l'option 1, il faut au préalable copier ce dossier `/public` sur votre serveur.

Et voilà !

## Contribuer au développement

### Comment ça marche, dis ?

Cette appli est développée avec [Svelte](https://svelte.dev). Le code est compilé à l'aide
de [Webpack](https://webpack.js.org/).

On a installé [Prettier](https://prettier.io) pour faire du joli code: pour formater tout le projet il suffit de se
mettre à la racine et de lancer `npm run prettier`.

### Lancer l'application en local

#### Pré-requis

Cette appli utilise `npm` pour fonctionner, vous devez l'installer sur votre machine d'abord.

#### Installation du projet

A la racine du projet, lancer l'installation des dépendances :

```bash
npm install
```

Il y aura quelques warnings, mais rien de grave. N'y faites pas attention :)

#### Lancement

Démarrez tout simplement le serveur en mode "développement" :

```bash
npm run dev
```

Il suffit ensuite de se rendre sur [http://localhost:8080/](http://localhost:8080/) pour accéder à l'application.

### Déployer une version de test en deux minutes top chrono

Si besoin, vous pouvez aussi déployer très rapidement l'application sur une URL gratuite, en utilisant Vercel.

Vercel est un site qui permet de déployer très rapidement une application. Il faut passer par leur utilitaire `now` pour
en bénéficier.

Installez `now`:

```bash
npm install -g now

# ou si ça ne marche pas sans les privilèges:
sudo npm install -g now
```

Faites-vous un compte sur [Vercel](https://vercel.com/home) avec un email.

Puis depuis le dossier du projet, lancez la commande:

```bash
now
```

Le programme vous guidera pour déployer facilement.

## Sources originales du projet

Le projet part du modèle [`sveltejs/template-webpack`](https://github.com/sveltejs/template-webpack).

## Plus d'infos

Voir [ce dossier](https://drive.google.com/drive/folders/1QwMRRijALMZOFP2wFEHkjANOM2cdkT7G) pour plus d'infos concernant le projet.
